package main

import (
	"database/sql"
	"fmt"
	"log"
	"reflect"

	"gitlab.com/bulldogcreative/yapm/pkg/datastore"
	"gitlab.com/bulldogcreative/yapm/pkg/labels"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	db, err := sql.Open("mysql", "bulldog:password@tcp(192.168.1.98:3306)/go")
	if err != nil {
		log.Println(err)
	}
	defer db.Close()

	dbService := datastore.MysqlService{
		Database: db,
	}

	fmt.Println(reflect.TypeOf(dbService))

	ls := labels.Label{
		Database: &dbService,
	}

	id := ls.Create("New")
	fmt.Println(id)
	ls.Delete(id)
}
