package labels

import (
	"gitlab.com/bulldogcreative/yapm/pkg/datastore"
)

// LabelService defines the methods it has.
type LabelService interface {
	Create(name string) int64
	Delete(id int)
	Apply()
	Remove()
}

// Label stuff
type Label struct {
	Database datastore.Database
}

// NewLabel is the construct for Label
func NewLabel(db datastore.Database) Label {
	label := Label{
		Database: db,
	}

	return label
}

// Create a label
func (l *Label) Create(name string) int64 {
	return l.Database.Create("labels", []string{
		"name=?",
	}, name)
}

// Delete a label
func (l *Label) Delete(id int64) {
	l.Database.Delete("labels", id)
}

// Apply a label. I would like for this to accept a specific type, that can be
// labeled. But since this is kinda specific to a password manager, I guess
// we aren't going to be labeling everything, and only some things.
func (l *Label) Apply() {

}

// Remove a label
func (l *Label) Remove() {

}
