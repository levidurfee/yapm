package datastore

// Database interface for stuff
type Database interface {
	Create(table string, fields []string, args ...interface{}) int64
	Retrieve(table string, id int) interface{}
	Update(table string, id int, fields []string, args ...interface{})
	Delete(table string, id int64)
}
