package datastore

import (
	"database/sql"
	"log"
	"strings"
)

// MysqlService stuff
type MysqlService struct {
	Database *sql.DB
}

// Create records into a table
func (s *MysqlService) Create(table string, fields []string, args ...interface{}) int64 {
	stmt, err := s.Database.Prepare(`INSERT INTO ` + table + ` SET ` + strings.Join(fields, ", "))

	if err != nil {
		log.Println(err)
	}

	res, err := stmt.Exec(args...)
	if err != nil {
		log.Println(err)
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Println(err)
	}

	return id
}

// Retrieve a record
func (s *MysqlService) Retrieve(table string, id int) interface{} {
	stmt, err := s.Database.Prepare(`SELECT * FROM ` + table + ` WHERE id=?`)
	if err != nil {
		log.Println(err)
	}

	res, err := stmt.Exec(id)
	if err != nil {
		log.Println(err)
	}

	return res
}

// Update a record
func (s *MysqlService) Update(table string, id int, fields []string, args ...interface{}) {

}

// Delete a record from the table.
func (s *MysqlService) Delete(table string, id int64) {
	stmt, err := s.Database.Prepare(`DELETE FROM ` + table + ` WHERE id=?`)
	if err != nil {
		log.Println(err)
	}
	_, err = stmt.Exec(id)
	if err != nil {
		log.Println(err)
	}
}
